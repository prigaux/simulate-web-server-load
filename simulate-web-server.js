//@ts-check

/**
 * @typedef {Object} inner_state
 * @property {string} id
 * @property {Object.<string,string>} css
 */

/**
 * @typedef {Object} one_state
 * @property {string} id
 * @property {string} description
 * @property {boolean=} hidden
 * @property {number=} max_requests
 * @property {request_next=} next_if_max
 * @property {inner_state[]} inner_states
 * @property {Object.<string,Object.<string,string>> =} inner_state2css
 * @property {Object.<string,number> =} current_requests  
 */

/** 
* @typedef {Object} request_next 
* @property {Object.<string, string>} states
* @property {number=} time
* @property {request_next=} next
*/

/** 
 * @typedef {Object} request 
 * @property {string} client
 * @property {string} path
 * @property {Object.<string, string>} states
 * @property {number} time
 * @property {request_next} next
 */

/**
 * @param {Object.<string,one_state>} id2state
 * @param {Object.<string, string>} states
 * @param {number} delta
 */
const update_current_requests = (id2state, states, delta) => {
    for (let state in states) {
        if (state === 'keepalive') continue;
        if (!id2state[state]) {
            console.error("undefined state " + state);
            continue;
        }
        let current_requests = id2state[state].current_requests;
        current_requests[states[state]] += delta;
        current_requests.all += delta;
    }
};

/**
 * @param {Object.<string,one_state>} id2state
 * @param {Object.<string, string>} current
 * @param {Object.<string, string>} wanted
 * @returns {one_state}
 */
const blocking_state = (id2state, current, wanted) => {
    if (wanted.finished) return null;
    for (let id in wanted) {
        if (id === 'keepalive') continue;
        if (current[id]) continue;
        const state = id2state[id];
        if (!state) console.error("invalid state id", id, current, wanted);
        if (state.current_requests.all === state.max_requests) {
            //console.log("can't evolve because of state " + id);
            return state;
        }
    }
    return null;
};

/**
 * @typedef {Object} queue_elt
 * @property {() => boolean} will_do
 * @property {request} request
 */
/** 
 * @typedef {Object} that
 * @property {queue_elt[]} queue
 * @property {Object.<string, one_state>} id2state
 * @property {request[]} requests
 * @property {number} requests_ignored
 * @property {Object.<string,number>} nb_finished
 * @property {number} slow_motion_factor
 * @property {number} start_time
 * @property {number} duration
 */

/**
 * @param {that} that
 */
function advance_queue(that) {
    const len = that.queue.length;
    that.queue = that.queue.filter(({ will_do }) => !will_do());
    if (that.queue.length < len) return advance_queue(that);
};

const mostly_finished = (states) => states.keepalive || states.finished;

/**
 * @param {that} that
 * @param {request} request
 */
function evolve(that, request) {
    update_current_requests(that.id2state, request.states, 1);
    const will_do = () => {
        let next = request.next;
        const blocked = blocking_state(that.id2state, request.states, request.next.states);
        if (blocked) {
            if (blocked.next_if_max) {
                next = blocked.next_if_max;
            } else {
                return false;
            }
        }
        update_current_requests(that.id2state, request.states, -1);
        //const states_to_string = (states) => Object.entries(states).map(([k, v]) => `${k}(${v})`);
        //console.log(JSON.stringify(that.queue.map(q => q.request.client)));
        //console.log(`${new Date().getTime() - that.start_time}: evolving ${request.client} ${request.path}: ${states_to_string(request.states)} => ${states_to_string(request.next.states)} after ${request.time}ms`);
        const prev_states = request.states;
        Object.assign(request, next);
        const finished = mostly_finished(request.states);
        if (finished && !mostly_finished(prev_states)) {
            that.nb_finished[finished]++;
            that.duration = (new Date().getTime() - that.start_time) / that.slow_motion_factor;
        }
        if (!request.states.finished) {
            evolve(that, request);
        }
        return true;
    };
    if (request.time === 0) {
        // try to evolve it synchroneously
        if (will_do()) {
            return;
        } else {
            // blocked on max, queue it
        }
    }
    setTimeout(_ => {
        that.queue.push({ request, will_do });
        advance_queue(that);
    }, request.time * that.slow_motion_factor);
}

/**
 * @param {that} that
 * @param {request} request
 */
function may_add_request(that, request) {
    if (blocking_state(that.id2state, {}, request.states)) {
        that.nb_finished.ignored++;
    } else {
        evolve(that, request);
        that.requests.push(request);
    }
}

const toObject = (list, f) => {
    let o = {};
    list.forEach(e => {
        const [k, v] = f(e);
        o[k] = v;
    })
    return o;
}

Vue.component('simulation', {
    template: `
  <div class="simulation">
    Temps : {{(duration/1000).toFixed(3)}}s
    <br> <span v-if="nb_finished.success">Requêtes terminées : {{nb_finished.success}}</span>
    <br> <span v-if="nb_finished.error">Requêtes en erreur : {{nb_finished.error}}</span>
    <br> <span v-if="nb_finished.ignored">Requêtes ignorées car backlog plein : {{nb_finished.ignored}}</span>
    <br>
    <table>
        <tr>
            <th><span v-if="details">Requests</span></th>
            <th v-for="field in visible_states">
                {{field.description}}
            </th>
        </tr>
        <tr>
            <th></th>
            <th v-for="field in visible_states">
                <div class="progress" :style="{ width: field.max_requests + 'rem'}">
                    <div v-for="inner_state in field.inner_states"
                        :style="{ width: '' + Math.floor(100 * (field.current_requests[inner_state.id] / field.max_requests)) + '%', ...inner_state.css }">
                    </div>
                </div>
            </th>
        </tr>
        <tr>
            <th></th>
            <th v-for="field in visible_states">
                <span v-for="(nb, inner_state) in field.current_requests" v-if="inner_state !== 'all' && inner_state !== field.id && nb">
                    {{inner_state}}: {{nb}}<br>
                </span>
                <!-- max:{{field.max_requests}} <br> -->
            </th>
        </tr>
        <tr v-for="request in requests" v-if="!request.states.finished && details">
            <td>{{request.client}} {{request.path}}</td>
            <td v-for="field in visible_states">
                <span :style="{ background: field.inner_state2css[request.states[field.id]].background }" v-if="request.states[field.id]">
                    {{request.states[field.id]}}
                </span>
            </td>
        </tr>
    </table>
  </div>`,
    props: ['params'],
    data() {
        /** @type { one_state[] } */
        const states = this.params.list_states.map(field => {
            field.inner_state2css = toObject(field.inner_states, ({ id, css }) => [id, css]);
            const current_requests = toObject([...field.inner_states.map(inner => inner.id), 'all'], s => [s, 0]);
            return { ...field, current_requests };
        });
        const id2state = toObject(states, field => [field.id, field]);

        return {
            queue: [],
            requests: [],
            nb_finished: { ignored: 0, success: 0, error: 0 },
            id2state,
            states,

            start_time: new Date().getTime(),
            duration: 0,
        };
    },
    computed: {
        slow_motion_factor() {
            return this.params.slow_motion_factor;
        },
        visible_states() {
            return this.states.filter(state => !state.hidden);
        },
    },
    mounted() {
        this.params.launch_clients(this, request => may_add_request(this, request));
    },
});

const times = {
    php: 400,
    keepalive: 5000,
    static_file: 10,
    static_file_slow: 1000,
}

const css_red_left = { background: 'red', float: 'left' };
const css_red_right = { background: 'red', float: 'right' };
const css_blue_right = { background: 'blue', float: 'right' };

const keepalive_inner_state = { id: 'keepalive', css: { background: 'green', float: 'left' } };
const static_file_inner_state = { id: 'static file', css: { background: 'pink' } };
const backlog_inner_state = { id: 'backlog', css: css_blue_right };

/** 
 * @type {Object.<string,one_state>}
 */
const std_states = {
    backlog: {
        id: "backlog", description: "Backlog", max_requests: 14, inner_states: [backlog_inner_state],
    },
    fpm_backlog: {
        id: "fpm_backlog", description: "FPM Backlog", max_requests: 14, inner_states: [backlog_inner_state],
    },
    nginx_proxy: {
        id: "nginx", description: "Nginx", max_requests: 20, next_if_max: { states: { finished: 'error' } }, inner_states: [
            { id: 'proxy', css: css_red_right },
            { id: 'proxy_fcgi', css: css_red_right },
            keepalive_inner_state,
            static_file_inner_state,
        ]
    },
    apache_proxy: {
        id: "apache", description: "Apache / mpm-worker", max_requests: 20, inner_states: [
            { id: 'proxy', css: css_red_right },
            { id: 'proxy_fcgi', css: css_red_right },
            { id: 'proxy_fcgi_queue', css: css_blue_right },
            keepalive_inner_state,
            static_file_inner_state,
        ]
    },
    apache_proxy_max: {
        id: "apache_proxy", description: "Apache / proxy", max_requests: 5, inner_states: [
            { id: 'proxy_fcgi', css: {} },
        ], hidden: true,
    },
    apache_mod_php: {
        id: "apache", description: "Apache / mod_php", max_requests: 4, inner_states: [
            { id: 'mod_php', css: css_red_right },
            keepalive_inner_state,
            static_file_inner_state,
        ]
    },
    fpm: {
        id: "fpm", description: "PHP FPM", max_requests: 4, inner_states: [
            { id: 'fpm', css: css_red_left },
        ]
    },
};

const with_backlog = (next) => ({
    states: { backlog: 'backlog' },
    time: 0,
    next,
});

/** @type {(client: string) => request_next} */
const success = (client) => ({
    states: { finished: 'success' },
});

/** @type {(client: string) => request_next} */
const keep_alive_and_success = (client) => ({
    states: { apache: 'keepalive', keepalive: 'success' },
    time: times.keepalive,
    next: success(client)
});

/**
 * @type {Object.<string, (_ : (client: string) => request_next, client: string) => request>}
 */
const new_client = {
    static_file: (finish, client, serverName) => ({
        client, path: '/x.html',

        ...with_backlog({
            states: { [serverName]: 'static file' },
            time: client.match(/slow/) ? times.static_file_slow : times.static_file,
            next: finish(client)
        })
    }),

    proxied: (finish, client, serverName) => ({
        client, path: '/x',

        ...with_backlog({
            states: { [serverName]: 'proxy' },
            time: times.php,
            next: finish(client)
        })
    }),

    apache_mod_php: (finish, client) => ({
        client, path: '/x.php',

        ...with_backlog({
            states: { apache: 'mod_php' },
            time: times.php,
            next: finish(client)
        })
    }),

    apache_fpm: (finish, client) => ({
        client, path: '/x.php',

        ...with_backlog({
            states: { apache: 'proxy_fcgi_queue' },
            time: 0,
            next: {
                states: { apache: 'proxy_fcgi', apache_proxy: 'proxy_fcgi', fpm_backlog: 'backlog' },
                time: 0,
                next: {
                    states: { apache: 'proxy_fcgi', apache_proxy: 'proxy_fcgi', fpm: 'fpm' },
                    time: times.php,
                    next: finish(client)
                }
            }
        })
    }),

    nginx_fpm: (finish, client) => ({
        client, path: '/x.php',

        ...with_backlog({
            states: { nginx: 'proxy_fcgi', fpm_backlog: 'backlog' },
            time: 0,
            next: {
                states: { nginx: 'proxy_fcgi', fpm: 'fpm' },
                time: times.php,
                next: finish(client)
            }
        })
    }),
};

const launch_clients = (opts, clients_new) => (that, may_add_request) => {
    let spawnId;
    let count = 0;
    const spawn = _ => {
        if (count++ < opts.nb_clients) {
            for (let new_ of clients_new) {
                const client = (opts.is_slow && opts.is_slow(count) ? 'slow' : 'A') + count;
                may_add_request(new_(client));
            }
        } else {
            clearInterval(spawnId);
        }
    }
    spawn();
    spawnId = setInterval(spawn, (1000 / opts.reqs_per_seconds) * that.slow_motion_factor);
};

Vue.component('simulate-keepalive-issue', {
    template: `
<div>
    Avec mod_php, le KeepAlive (en vert) entre en concurrence avec les processus PHP (en rouge)
    <br>
    <small>
      <br>PHP request: {{times.php}}ms
      <br>HTTP KeepAlive: {{times.keepalive / 1000}}s
    </small>
    <p></p>
    <simulation :params="params_mod_php"></simulation>
    <simulation :params="params_fpm"></simulation>
</div>`,
    methods: {
        to_params(list_states, new_) {
            return {
                slow_motion_factor: 1,
                list_states,
                launch_clients: launch_clients(
                    { nb_clients: 10, reqs_per_seconds: 5 },
                    [(client) => new_(keep_alive_and_success, client)]),
            };
        },
    },
    computed: {
        params_mod_php() {
            return this.to_params([
                std_states.backlog,
                std_states.apache_mod_php,
            ], new_client.apache_mod_php);
        },
        params_fpm() {
            return this.to_params([
                std_states.backlog,
                std_states.apache_proxy,
                std_states.apache_proxy_max,
                std_states.fpm_backlog,
                std_states.fpm,
            ], new_client.apache_fpm);
        },
    },
});

Vue.component('simulate-slow-clients-issue', {
    template: `
<div>
    Avec mod_php, les gros fichiers ou connexions lentes (en rose) entre en concurrence avec les processus PHP (en rouge)
    <br>
    <small>
      <br>PHP request: {{times.php}}ms
      <br>Slow requests: {{times.static_file_slow}}ms
    </small>
    <p></p>
    <simulation :params="params_mod_php"></simulation>
    <simulation :params="params_fpm"></simulation>
</div>`,
    methods: {
        to_params(list_states, new_) {
            return {
                slow_motion_factor: 2,
                list_states,
                launch_clients: launch_clients(
                    { nb_clients: 10, reqs_per_seconds: 10, is_slow: _ => true },
                    new_.map(new_ => (client) => new_(success, client, 'apache'))),
            };
        },
    },
    computed: {
        params_mod_php() {
            return this.to_params([
                std_states.backlog,
                std_states.apache_mod_php,
            ], [ new_client.apache_mod_php, new_client.static_file ]);
        },
        params_fpm() {
            return this.to_params([
                std_states.backlog,
                std_states.apache_proxy,
                std_states.apache_proxy_max,
                std_states.fpm_backlog,
                std_states.fpm,
            ], [ new_client.apache_fpm, new_client.static_file ]);
        },
    },
});

Vue.component('simulate-apache-vs-nginx-overflow', {
    template: `
<div>
    nginx n'utilise pas le backlog pour mettre en attente. 
    <p></p>
    En cas de dépassement de <tt>worker_connections</tt>, nginx prévient par un "worker_connections are not enough" dans error.log, mais une réponse vide et aucune entrée dans le access log !!
    Il faut s'assurer d'avoir un <tt>worker_connections</tt> élevé !

    <p></p>
    <simulation :params="params_apache"></simulation>
    <simulation :params="params_nginx"></simulation>
</div>`,
    methods: {
        to_params(list_states, new_, serverName) {
            return {
                slow_motion_factor: 10,
                list_states,
                launch_clients: launch_clients(
                    { nb_clients: 30, reqs_per_seconds: 100, is_slow: _ => true },
                    [(client) => new_(success, client, serverName)]),
            };
        },
    },
    computed: {
        params_apache() {
            return this.to_params([
                std_states.backlog,
                std_states.apache_proxy,
            ], new_client.proxied, 'apache');
        },
        params_nginx() {
            return this.to_params([
                std_states.backlog,
                std_states.nginx_proxy,
            ], new_client.proxied, 'nginx');
        },
    },
});

Vue.component('simulate-apache-vs-nginx-queue', {
    template: `
<div>
    En utilisant <tt>max</tt>, apache met les requêtes en attente. L'option <tt>queue</tt> de nginx est payante. Voici la comparaison avec et sans queue.
    <p></p>
    <simulation :params="params_nginx"></simulation>
    <simulation :params="params_apache"></simulation>
</div>`,
    methods: {
        to_params(list_states, new_) {
            return {
                slow_motion_factor: 8,
                list_states,
                launch_clients: launch_clients(
                    { nb_clients: 20, reqs_per_seconds: 40 },
                    [(client) => new_(success, client)]),
            };
        },
    },
    computed: {
        params_apache() {
            return this.to_params([
                std_states.backlog,
                std_states.apache_proxy,
                std_states.apache_proxy_max,
                std_states.fpm_backlog,
                std_states.fpm,
            ], new_client.apache_fpm);
        },
        params_nginx() {
            return this.to_params([
                std_states.backlog,
                std_states.nginx_proxy,
                std_states.fpm_backlog,
                std_states.fpm,
            ], new_client.nginx_fpm);
        },
    },
});

Vue.mixin({
    data() {
        return {
            times,
            details: false,
        };
    },
});
new Vue({
    el: '#app',
    mounted() {
        this.update_simulation_name();
        window.onhashchange = () => this.update_simulation_name();
    },
    data() {
        return {
            simulation_name: '',
        };
    },
    computed: {
        tag() {
            return 'simulate-' + this.simulation_name;
        },
        invalid() {
            return !Vue.options.components[this.tag];
        },
        valid_simulation_names() {
            let r = [];
            for (let n in Vue.options.components) {
                const m = n.match(/^simulate-(.*)/);
                if (m) r.push(m[1]);
            }
            return r;
        },
    },
    methods: {
        update_simulation_name() {
            this.simulation_name = location.hash.replace(/^#/, '');
        },
    },
});
